var express = require('express'); // For RESTful services
var bodyParser = require('body-parser'); // For parsing JSON requests

var app = express();

// Parse application/json
app.use(bodyParser.json());
app.use (function (error, req, res, next) {
    res.json(400, { "error": "Could not decode request: JSON parsing failed" });
});

// Post to root url
app.post('', function(req, res) {
    var data = req.body || {};

    res.type('application/json');
    // If the request isn't a JSON object or doesn't have a payload 
    // (or has an empty payload ) return an error with status 400
    if (!Object.keys(data).length || !data.payload || !data.payload.length) {
        res.json(400, { "error": "Could not decode request: JSON parsing failed" });
        return;
    }
    
    // Read through the payload data 
    var payload = data.payload;
    var responseData = [];
    try {
        for (var i = 0; i < payload.length; i++) {
            var show = payload[i];
            // If the show criteria are met add the show details to the responseData
            if (show.episodeCount > 0 && show.drm)
                responseData.push({ 'image': show.image.showImage, 'slug': show.slug, 'title': show.title });
        };
    }
    catch (ex) { // Something unexpected has happened
        res.json(400, { "error": "Could not decode request: JSON parsing failed" });
        return;
    }

    // Send back the response with data
    res.json(200, { "response": responseData });

}); 

// Start listening
app.listen(process.env.PORT || 80);