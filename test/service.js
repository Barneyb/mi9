var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
// The URL to test to (using heroku at the moment)
var url = "http://localhost/";
//var url = "http://enigmatic-depths-7861.herokuapp.com/";
// The data used for testing
var testdata = require('./testdata.json'); 
 
  // Create tests for the service module
  describe('Service', function() {

    // Everything should work here
    it('should return provided data', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send(testdata.provided.request)
      .expect(200) //Status code
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            // Eql compares object content, not object reference 
            res.body.response.should.eql(testdata.provided.response);
            done();
          });
      });

    // In this dataset none of the shows have drm:true
    it('should return no data due to no drm match', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send(testdata.nodrm.request)
      .expect(200)  // Expect the API to return 200
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            // Eql compares object content, not object reference 
            res.body.response.should.eql(testdata.nodrm.response);
            done();
          });
      });
    // In this dataset none of the shows have episodes > 0
    it('should return no data due to no episode counts', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send(testdata.noepisodes.request)
      .expect(200) // Expect the API to return 200
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            // Eql compares object content, not object reference 
            res.body.response.should.eql(testdata.noepisodes.response);
            done();
          });
      });
   
   // POSTS a string instead of JSON data
   it('should return 400 due to invalid JSON', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send('This is not JSON')
      .expect(400) // Expect the API to return 400
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            // Eql compares object content, not object reference 
            res.body.error.should.eql("Could not decode request: JSON parsing failed");
            done();
          });
      });

   // POSTS no data
   it('should return 400 due to no data', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send()
      .expect(400) // Expect the API to return 400
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            res.body.error.should.eql("Could not decode request: JSON parsing failed");
            done();
          });
      });

   // POSTS data without a payload key
   it('should return 400 due no payload key', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send({ key: 'value' })
      .expect(400) // Expect the API to return 400
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            res.body.error.should.eql("Could not decode request: JSON parsing failed");
            done();
          });
      });

   // POSTS an empty payload
   it('should return 400 due to empty payload', function(done) {        
      request(url)
      .post('')
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .send({ 'payload': [] })
      .expect(400) // Expect the API to return 400
      .end(function(err, res) {
            if (err) {
              throw err;
            }
            res.body.error.should.eql("Could not decode request: JSON parsing failed");
            done();
          });
      });
});